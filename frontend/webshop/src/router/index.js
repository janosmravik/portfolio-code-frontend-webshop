
/*fix marad*/

import Vue from 'vue'
import VueRouter from 'vue-router'


/*nem fix*/

import HomeView from '../views/HomeView.vue'
import Login from '../views/LoginView'
import Registration from "@/views/RegistrationView";
import AuthLayout from "@/layouts/AuthLayout";
import ProductsOverviewLayout from "@/layouts/ProductsOverviewLayout";
import ProductsPageLayout from "@/layouts/ProductsPageLayout";
import ShoppingCartView from "@/layouts/ShoppingCartView";
import TestLayout from "@/layouts/TestLayout";






/*import Product from '../views/TestProduct.vue'*/
// import Category from '../views/Category.vue'
import Search from '../views/Search.vue'
import AboutUs from "@/views/AboutUs";
import TeamView from "@/views/TeamView";


import MyAccount from '../views/MyAccount.vue'
import Checkout from '../layouts/Checkout.vue'
import Success from '../views/Success.vue'
import ContactFormular from "@/components/ContactFormular";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeView
    },
    {
        path: '/product/:id',
        name: 'product',
        component: ProductsOverviewLayout
    },
    {
        path: '/aboutus',
        name: 'aboutus',
        component: AboutUs
    },
    {
        path: '/ourteam',
        name: 'teamView',
        component: TeamView
    },
    {
        path: '/allproducts',
        name: 'allproducts',
        component: ProductsPageLayout
    },
    {
        path: '/test',
        name: 'test',
        component: TestLayout
    },
    {
        path: '/login',
        component: AuthLayout,
        children: [
            {
                path: '',
                name: 'login',
                component: Login
            },
            {
                path: '/registration',
                name: 'registration',
                component: Registration
            }
        ]
    },
    {
        path: '/bag',
        name: 'shoppingcart',
        component: ShoppingCartView
    },
    {
        path: '/bag/checkout',
        name: 'checkout',
        component: Checkout
    },
    {
        path: '/my-account',
        name: 'MyAccount',
        component: MyAccount,
        meta: {
            requireLogin: true
        }
    },
    {
        path: '/search',
        name: 'Search',
        component: Search
    },
    {
        path: '/bag/success',
        name: 'Success',
        component: Success
    },
    {
        path: '/cart/checkout',
        name: 'Checkout',
        component: Checkout,
        meta: {
            requireLogin: true
        }
    },

    {
        path: '/contactus',
        name: 'ContactFormular',
        component: ContactFormular
    },
    /*{
        path: '/:category_slug/:product_slug',
        name: 'Product',
        component: Product
    },*/
   /* {
        path: '/:category_slug',
        name: 'Category',
        component: Category
    }*/


]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
