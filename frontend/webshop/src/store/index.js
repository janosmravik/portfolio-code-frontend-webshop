import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: null,

        products: [],

        users: [],

        userData: [],

        productDetails: [],

        cart: {
            items: [],
            globalPrice: 0.0,
        },
        token: '',
        isLoading: false,


    },
    getters: {},
    mutations: {
        setUser(state, user) {
            state.user = user
            console.log(user)
        },

    setProducts(state, products){
      state.products = products
    },
        setAllUsers(state, users) {
            state.users = users
        },
        setUserData(state, userData) {
            state.userData = userData
        },
        setProductDetails(state, productDetails) {
            state.productDetails = productDetails
        },
        pushProductToCart(state, productId){
            state.cart.push({
                id:productId,
                quantity: 1

            })
        } ,
        incrementItemQuantity(state, cartItem) {
            cartItem.quantity++
        },
        decrementProductInventory(state, product){
            product.inventory--
        },

        addProductNew(state,product){
            state.products.push(product)
        },
        /*deleteProduct(state, productId) {
            const indexOfProduct = state.admin.products.findIndex()
            state.admin.products.splice(indexOfProduct, 1)
        }*/
        initializeStore(state) {
            if (localStorage.getItem('cart')) {
                state.cart = JSON.parse(localStorage.getItem('cart'))
            } else {
                localStorage.setItem('cart', JSON.stringify(state.cart))
            }

            if (localStorage.getItem('token')) {
                state.token = localStorage.getItem('token')
                state.isAuthenticated = true
            } else {
                state.token = ''
                state.isAuthenticated = false
            }
        },
        addToCart(state, item) {
            console.log(item)
            const exists = state.cart.items.filter(i => i.product.id === item.product.id)
            if (exists.length) {
                exists[0].quantity = parseInt(exists[0].quantity) + parseInt(item.quantity)
            } else {
                state.cart.items.push(item)
            }

            console.log(this.state.cart.items)
            localStorage.setItem('cart', JSON.stringify(state.cart))
        },
        setIsLoading(state, status) {
            state.isLoading = status
        },
        clearCart(state) {
            console.log("in store clearCart mutation")
            state.cart.items = []
            localStorage.setItem('cart', JSON.stringify(state.cart))
        },

    },
    /*  get login*/
    actions: {
        async performLoginAction(context, credentials) {
            var basicAuth = 'Basic ' + btoa(credentials.email + ':' + credentials.password)
            const response = await Vue.axios.get('/signedInUser', {
                headers: {'Authorization': basicAuth}
            })
            console.log(response)
            context.commit('setUser', response.data)

        },
        /*  post registration*/
        async performRegistrationAction(context, credentials) {
            console.log("reg action starting, store reached")
            console.log("posting this to backend" , credentials)
            context = await Vue.axios.post('/newAccount', credentials)
        },
        /*get all Products*/
        async loadAllProducts(context) {
            const productResponse = await Vue.axios.get('/products')
            context.commit('setProducts', productResponse.data)
            console.log(productResponse.data)
        },
        async loadAllUsers(context) {
            const allUsersResponse = await Vue.axios.get('/accounts')
            context.commit('setAllUsers', allUsersResponse.data)
            console.log(allUsersResponse.data)
        },
        async loadUserData(context, email) {
            const UserDataResponse = await Vue.axios.get('/accountByEmail/' + email)
            context.commit('setUserData', UserDataResponse.data)
            console.log(UserDataResponse.data)
        },

        async getSignedInUser(context) {
            const response = await Vue.axios.get('/signedInUser')
            context.commit('setUser', response.data)
            console.log(response.data + " response from backend")
    },
    async logout(context) {
      await fetch('/logout', {
        redirect: 'manual'
      })
      context.commit('setUser', null)
    },

        //get product details
    async getProductDetails(context,productId) {

      console.log("store: getProductDetails" + productId)
      const response = await Vue.axios.get('/productDetails/' + productId);
      console.log(response , " response from backend")
      context.commit('setProductDetails', response.data)
      console.log(response.data , " response.data from backend")
      console.log("action from store done")
    },
/*
        addProductToCart (context, product) {
            if (product.inventory > 0) {
                const cartItem = context.state.cart.find(item => item.id === product.id)
                //find cartItem
                if (!cartItem) {
                    context.commit('pushProductToCart', product.id)
                } else {
                    //incrementItemQuantity
                    context.commit('incrementItemQuantity', cartItem)
                }
                context.commit('decrementProductInventory', product)
            }

            },*/

        async clearCart(context) {
            context.commit("clearCart")
        },

        //delete product
      async DeleteProduct(store, id) {
          console.log("dispatch was ok, payload is:", id)
            const response = await Vue.axios.delete('/deleteProduct/' + id);
          console.log("delete http finished")
            console.log("backend response is: " ,response.data);
          console.log("now loading all products again")
          this.dispatch("loadAllProducts");

        },

        //add new product, if admin view
        async addProductToAdminView(store, credentials) {
            console.log(credentials)
            const response = await Vue.axios.post('/newProduct', credentials);

            console.log('addProductNew', response.data);
            store.commit('addProductNew', response.data)
            this.dispatch("loadAllProducts");
        },

    }
})
